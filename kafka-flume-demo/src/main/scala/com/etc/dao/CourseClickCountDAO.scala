package com.etc.dao

import com.etc.domain.CourseClickCount
import com.etc.utils.HBaseUtils
import org.apache.hadoop.hbase.util.Bytes
import scala.collection.mutable.ListBuffer

/**
  * 实战课程点击数-数据访问层
  */
object CourseClickCountDAO {
  val tableName = "imooc_course_clickcount"             //表名
  val cf = "info"                                       //列族
  val qualifer = "click_count"                          //

  /**
    * 保存数据到HBase
    *
    * @param list CourseClickCount集合
    */

  def save(list: ListBuffer[CourseClickCount]): Unit = {
    val table = HBaseUtils.getInstance().getTable(tableName)

    for (ele <- list) {
       table.incrementColumnValue(Bytes.toBytes(ele.day_course),
        Bytes.toBytes(cf),
        Bytes.toBytes(qualifer),
        ele.click_count)
    }
  }

  def main(args: Array[String]): Unit = {
  }
}
