package com.etc.utils

import java.util.Date
import org.apache.commons.lang3.time.FastDateFormat
/**
  * 日期时间工具类
  *
  * 把这种格式2019-04-04 09:55:01转化为 20190404095501
  */
object DateUtils {
  val YYYYMMDDHHMMSS_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss")
  val TARGE_FORMAT = FastDateFormat.getInstance("yyyyMMddHHmmss")


  def getTime(time: String) = {
    YYYYMMDDHHMMSS_FORMAT.parse(time).getTime
  }

  def parseToMinute(time :String) = {
    TARGE_FORMAT.format(new Date(getTime(time)))
  }


  def main(args: Array[String]): Unit = {

    println(parseToMinute("2019-04-04 09:55:01"))

  }

}
