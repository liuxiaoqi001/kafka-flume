package com.example.echares.controller;


import com.example.echares.dao.CourseClickCountDAO;
import com.example.echares.domian.CourseClickCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class HelloWorld {
    private static Map<String, String> courses = new HashMap<>();
    static {
        courses.put("112","Spark SQL慕课网日志分析");
        courses.put("128","10小时入门大数据");
        courses.put("145","深度学习之神经网络核心原理与算法");
        courses.put("146","强大的Node.js在Web开发的应用");
        courses.put("131","Vue+Django实战");
        courses.put("130","Web前端性能优化");
    }

    @Autowired
    private CourseClickCountDAO courseClickCountDAO;

    @GetMapping("demo")
    public ModelAndView str(){
        return new ModelAndView("demo");
    }

    @PostMapping("/course_clickcount_dynamic")
    @ResponseBody
    public List<CourseClickCount> courseClickCount() throws Exception {
        List<CourseClickCount> list = courseClickCountDAO.query("20190409");
        for(CourseClickCount model : list) {
            model.setName(courses.get(model.getName().substring(9)));
        }
        return list;
    }
}
