package com.example.echares;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EchartsApplication {

    private final static Logger log = LoggerFactory.getLogger(EchartsApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(EchartsApplication.class, args);
        log.error("********** spring boot success ****");
    }

}
