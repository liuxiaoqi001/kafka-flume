package com.example.echares;

import com.example.echares.dao.CourseClickCountDAO;
import com.example.echares.domian.CourseClickCount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EcharesApplicationTests {

    @Autowired
    private CourseClickCountDAO courseClickCountDAO;

    @Test
    public void contextLoads() throws Exception {
        List<CourseClickCount> query = courseClickCountDAO.query("20190409");
        for (CourseClickCount courseClickCount : query) {
            System.out.println(courseClickCount.getName());
        }
    }

}
